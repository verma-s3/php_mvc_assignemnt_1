<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services;

class ServicesController extends Controller
{
    
    /**
	 * services page
	 * @return view [description]
	 */
    public function services(){
    	$subtitle = 'Services';
    	$services = Services::tableshow();
    	return view('services',compact('subtitle','services'));
    }
    /**
	 * servicesDetail page to show detail of service
	 * @return view [description]
	 */
    public function servicesDetail($package_type){
    	$subtitle = 'Services';
    	$service = Services::where('package_type',$package_type)->firstOrFail();
    	return view('services_detail',compact('subtitle','service'));
    }
}
