<?php
// namspaces 
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{	
	
	/**
	 * home page
	 * @return view [description]
	 */
    public function home(){
    	$subtitle = 'Welcome';
    	return view('home',compact('subtitle','title'));
    }

    /**
	 * about page
	 * @return view [description]
	 */
    public function about(){
    	$subtitle = 'About Us';
    	return view('about',compact('subtitle'));
    }

    /**
	 * photo page
	 * @return view [description]
	 */
    public function photo(){
    	$subtitle = 'Photography';
    	return view('photo',compact('subtitle'));
    }


    /**
	 * contact page
	 * @return view [description]
	 */
    public function contact(){
    	$subtitle = 'Contact us:';
    	return view('contact',compact('subtitle'));
    }
}
