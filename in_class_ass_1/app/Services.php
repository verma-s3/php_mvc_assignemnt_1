<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{	
	/**
	 * Table show -- show the details of table
	 * @return records
	 */
    public static function tableShow(){
    	return self::latest()->get();
    }
}
